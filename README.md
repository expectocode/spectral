# Spectral

<a href='https://flathub.org/apps/details/org.eu.encom.spectral'><img width='240' alt='Get it on Flathub' src='https://flathub.org/assets/badges/flathub-badge-i-en.png'/></a>

> "Nobody can be told what the matrix is, you have to see it for yourself. "

Spectral is a glossy cross-platform client for Matrix, the decentralized communication protocol for instant messaging.

![Screenshot](https://gitlab.com/b0/spectral/raw/master/screenshots/1.png)
![Screenshot](https://gitlab.com/b0/spectral/raw/master/screenshots/2.png)
![Screenshot](https://gitlab.com/b0/spectral/raw/master/screenshots/3.png)
![Screenshot](https://gitlab.com/b0/spectral/raw/master/screenshots/4.png)

## Document

There is a separate document for Spectral, including installing, compiling, etc.

It is at [Spectral Doc](https://b0.gitlab.io/spectral-doc/)

## Contact

You can reach the maintainer at #spectral:matrix.org, if you are already on Matrix.

Also, you can file an issue at this project if anything goes wrong.

## Acknowledgement

This program uses libqmatrixclient library and some C++ models from Quaternion. 

[Quaternion](https://github.com/QMatrixClient/Quaternion)

[libqmatrixclient](https://github.com/QMatrixClient/libqmatrixclient)

This program includes the source code of hoedown.

[Hoedown](https://github.com/hoedown/hoedown)

## Donation

Donations are welcome! My Bitcoin wallet address is 1AmNvttxJ6zne8f2GEH8zMAMQuT4cMdnDN

## License

![GPLv3](https://www.gnu.org/graphics/gplv3-127x51.png)

This program is licensed under GNU General Public License, Version 3. 

Exceptions are src/notifications/wintoastlib.c and wintoastlib.h, which are from https://github.com/mohabouje/WinToast and licensed under MIT.
